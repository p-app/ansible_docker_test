## Ansible Docker Test

This python package is intended for creating set of files that will help test any ansible role.


### Installation
```bash
sudo -H pip install git+https://bitbucket.org/p-app/ansible_docker_test
```

### Usage
```bash
ansible_docker_test --help
```

### Compatibility
This package is python 2 and 3 both compatible.
