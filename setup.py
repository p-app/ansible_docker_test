from setuptools import find_packages, setup

setup(
    name='ansible_docker_test',
    version='0.3.1',
    packages=find_packages(),
    url='https://bitbucket.org/p-app/ansible_docker_test',
    license='MIT',
    author='Vladimir V. Pivovarov',
    author_email='944950@gmail.com',
    description='Infrastructure for testing ansible roles on docker container.',
    install_requires=['mako'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'ansible_docker_test = ansible_docker_test.script:run'
        ]
    }
)
