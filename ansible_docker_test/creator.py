from os import makedirs
from os.path import dirname, exists, join
from shutil import copytree, rmtree

from mako.template import Template
from pkg_resources import resource_filename

__all__ = [
    'AnsibleDockerTestCreator'
]


_TEMPLATES = {
    'dockerfile': 'Dockerfile',
    'inventory': 'inventory.ini',
    'start-container': 'start-container.sh',
    'stop-container': 'stop-container.sh',
    'run-test': 'run-test.sh',
    'playbook': 'playbook.yml'
}


class AnsibleDockerTestCreator(object):

    def __init__(self, settings):
        self._settings = settings

    @property
    def _root_dirpath(self):
        return join(
            self._settings['output'], 'tests'
        )

    @property
    def _keys_dirpath(self):
        return join(
            self._root_dirpath, 'keys'
        )

    def _ensure_destination(self):
        if not exists(self._keys_dirpath):
            makedirs(self._keys_dirpath)

    def _copy_keys(self):
        if exists(self._keys_dirpath):
            rmtree(self._keys_dirpath)

        copytree(
            resource_filename('ansible_docker_test', 'keys'),
            self._keys_dirpath
        )

    def _render_template(self, template_path, destination_path):
        template = Template(filename=template_path,
                            input_encoding='utf-8',
                            output_encoding='utf-8',
                            strict_undefined=False)

        out_dirpath = dirname(destination_path)
        if not exists(out_dirpath):
            makedirs(out_dirpath)

        with open(destination_path, 'wb') as f:
            f.write(template.render(**self._settings))

    def _render_test_templates(self):
        templates_dirpath = resource_filename(
            'ansible_docker_test', 'templates'
        )

        for template_name, dest_basename in _TEMPLATES.items():
            self._render_template(
                join(templates_dirpath, '%s.mako' % template_name),
                join(self._root_dirpath, dest_basename)
            )

    def run(self):
        self._ensure_destination()
        self._copy_keys()
        self._render_test_templates()

    __call__ = run
