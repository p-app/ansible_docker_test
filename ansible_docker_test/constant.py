from collections import namedtuple

__all__ = [
    'DOCKER_IMAGES'
]


DOCKER_IMAGES = namedtuple(
    'DOCKER_IMAGES',
    (
        'UBUNTU_1404',
        'UBUNTU_1604',
        'UBUNTU_1604_SYSTEMD',
        'DEBIAN_8',
        'DEBIAN_9'
    )
)(
    'ubuntu:14.04',
    'ubuntu:16.04',
    'solita/ubuntu-systemd:16.04',
    'debian:8',
    'debian:9',
)
