# coding=utf-8
from __future__ import absolute_import

import argparse
import os

from .constant import DOCKER_IMAGES
from .creator import AnsibleDockerTestCreator

__all__ = [
    'run'
]


def run():
    parser = argparse.ArgumentParser(
        description='Creator of test files set for any ansible role.'
    )

    parser.add_argument('-o', '--output', required=False, default=os.getcwd(),
                        help='Ansible role path, where "tests" folder will be '
                             'created.')

    parser.add_argument('-i', '--docker-image', required=False,
                        default=DOCKER_IMAGES.UBUNTU_1604,
                        choices=DOCKER_IMAGES,
                        help='Docker base image.')

    parser.add_argument('--systemd', required=False, default=False,
                        action='store_true',
                        help='Should docker image use systemd?')

    parser.add_argument('--add-sudo', required=False, action='store_true',
                        default=False,
                        help='Should we add sudoer user "user" for testing '
                             'purposes (besides root)?')

    parser.add_argument('--ssh-port', required=False, default=7922, type=int,
                        help='You will be able to connect to docker container '
                             'via ssh over this port on localhost.')
    parser.add_argument('--hosts-group', required=False,
                        default='docker_test',
                        help='Ansible host group name for inventory.')
    parser.add_argument('--abspath', required=False, action='store_true',
                        default=False, help='Should output path be absolute '
                                            'in created files?')
    parser.add_argument('-r', '--role-name', required=False, default=None,
                        help='Ansible role name. By default it will be got '
                             'from output path option.')
    parser.add_argument('-p', '--ports', required=False, default=[], nargs='*',
                        help='Additional tcp ports, that will be mapped to '
                             'localhost from container.')

    args = parser.parse_args()

    settings = vars(args)
    abspath = os.path.abspath(settings['output'])

    if args.abspath:
        settings['output'] = abspath

    if not args.role_name:
        # Detect role name from folder
        dirpath = abspath

        if not os.path.isdir(dirpath):
            dirpath = os.path.dirname(dirpath)

        settings['role_name'] = os.path.basename(dirpath)

    # Detect systemd image paremeters
    if args.docker_image == DOCKER_IMAGES.UBUNTU_1604_SYSTEMD:
        settings['systemd'] = True

    elif args.systemd is True:
        settings['docker_image'] = DOCKER_IMAGES.UBUNTU_1604_SYSTEMD

    AnsibleDockerTestCreator(settings).run()


if __name__ == '__main__':
    run()
