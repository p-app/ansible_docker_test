#!/usr/bin/env bash
# Generated by ansible_docker_test (https://bitbucket.org/p-app/ansible_docker_test)

cd "$( dirname "${"${BASH_SOURCE[0]}"}" )/.."

# it means, that you activate virtualenv or install ansible globaly
ansible-playbook -i tests/inventory.ini tests/playbook.yml -vv
